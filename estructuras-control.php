<!DOCTYPE html>
<html>
<head>
	<title></title>
</head>
<body>

<?php 

	/* 
		Las estructuras de control en lenguajes de programacion
		se utilizan para realizar operaciones por 
		ejemplo : selectivas, 
	*/
		$colores = array('blue','green','yellow','cyan','magenta','negro','blanco','green','yellow','cyan','magenta','negro','blanco');
		$x= 1;

		if ($x == 0) {
			echo "La variable es Cero";
		}
		else
		{

			echo "La variable no es CERO";
		}
		for ($i=0; $i <= 10; $i++) { 
			echo "<br />Los numeros naturales hasta el 10 = ".$i;
		}

		for ($i=0; $i < 8 ; $i++) { 
			echo "<br />Elemento $i=[".$colores[$i]."]";
			
		}

		for ($i=0; $i < count($colores); $i++) { 

			echo "<br /><h2>Color <h1 style='color:$colores[$i]'>$i: $colores[$i]</h1></h2>";
		}

		//EJERCICIOS

		$vector = array(0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20);

		for ($i=0; $i < count($vector); $i=$i+2) { 
			echo "<br />Multiplos de 2:" .$vector[$i];

		}


 ?>

</body>
</html>